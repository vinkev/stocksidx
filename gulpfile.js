var elixir = require('laravel-elixir');

/*
 |----------------------------------------------------------------
 | Have a Drink
 |----------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic
 | Gulp tasks for your Laravel application. Elixir supports
 | several common CSS, JavaScript and even testing tools!
 |
 */

elixir(function(mix) {
    mix.styles([
        "bootstrap-3.3.2/css/bootstrap.min.css",
        "bootstrap-lumen.min.css",
        "layout.css"
    ], 'public/css/app.css');

    mix.scripts([
        "jquery-2.1.3.min.js",
        "bootstrap.min.js",
        "main.js"
    ], 'public/js/app.js')
    .scripts([
        "highstock/highstock.js",
        "highstock/modules/exporting.js",
        "highstock/modules/technical-indicators.src.js",
        "highstock/modules/scalable-yaxis.js",
        "preload-loading.js"
    ], 'public/js/stock.js');

    mix.copy('resources/css/bootstrap-3.3.2/fonts', 'public/fonts');
    mix.copy('resources/images', 'public/images');
});
