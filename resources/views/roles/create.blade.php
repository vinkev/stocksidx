@extends('layout.master')

@section('title')
Role - Create
@stop

@section('content')
	<h1>Create Role</h1>
	@include('roles.form')
@stop