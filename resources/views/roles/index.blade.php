@extends('layout.master')

@section('title')
Roles
@stop

@section('content')
	<h1>List Roles</h1>
	<hr>
	<div class="panel panel-default">
		<div class="panel-heading">
			<a class="btn btn-primary" href="{{ action('RoleController@create')}}">Add New Role</a>
		</div>
	</div>

	<div class="table-responsive">
		<table class="table table-striped table-hover table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th>Display Name</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($roles as $role) 
				<tr>
					<td>{{$role->name}}</td>
					<td>{{$role->display_name}}</td>
					<td class="col-sm-1 nowrap">
						<a class="btn btn-warning btn-xs" href="{{ action('RoleController@edit', $role->id) }}" >Edit</a> 
			   			{!! Form::open(array('url' => action('RoleController@destroy', $role->id) , 'class' => 'form-delete')) !!}
			   			{!! Form::hidden('_method', 'delete') !!}
			        		<button type="submit" class="btn btn-danger btn-xs">Delete</button>
			    		{!! Form::close() !!}
			    	</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@include('layout.pagination', ['page' => $roles])
@stop