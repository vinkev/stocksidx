<?php 
if($role == "[]") {
	$formUrl = action('RoleController@store');
	$method = "post";
} else {
	$formUrl = action('RoleController@update', $role->id);
	$method = "put";
}
?>
<div class="well">
	{!! Form::open(array('url' => $formUrl, 'class' => 'form form-horizontal')) !!}
	{!! Form::hidden('_method',$method) !!}
	<div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
		{!! Form::label("name", "Name", ['class' => 'col-md-2 control-label'])!!}
		<div class="col-md-10">
			{!! Form::text('name', $role->name, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
			@if ($errors->has('name'))
			{!! $errors->first('name', '<small class=error>:message</small>') !!}
			@endif
		</div>
	</div>
	<div class="form-group {!! $errors->has('display_name') ? 'has-error' : '' !!}">
		{!! Form::label("display_name", "Display Name", ['class' => 'col-md-2 control-label'])!!}
		<div class="col-md-10">
			{!! Form::text('display_name', $role->display_name, ['class' => 'form-control', 'placeholder' => 'Display Name']) !!}
			@if ($errors->has('display_name'))
			{!! $errors->first('display_name', '<small class=error>:message</small>') !!}
			@endif
		</div>
	</div>
	<div class="form-group">
		{!! Form::label("description", "Description", array('class' => 'col-md-2 control-label'))!!}
		<div class="col-md-10">
			{!! Form::textarea('description', $role->description, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
		</div>
	</div>
	<div class="form-group">
		<div class="col-md-offset-2 col-md-10">
			{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
			<a class="btn btn-danger" href="{{ action('RoleController@index') }}">Cancel</a>
		</div>
	</div>
	{!! Form::close() !!}
</div>