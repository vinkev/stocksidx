@extends('layout.master')

@section('title')
Role - Edit
@stop

@section('content')
	<h1>Edit Role</h1>
	@include('roles.form')
@stop