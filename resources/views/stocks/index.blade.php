@extends('layout.master')

@section('title')
Stocks
@stop

@section('content')
	<h1>List Stocks</h1>
	<hr>
	<div class="panel panel-default">
		<div class="panel-heading">
			<a class="btn btn-primary" href="{{ action('StockController@create')}}">Add New Inventory</a>
		</div>
	</div>

	<div class="table-responsive">
		<table class="table table-striped table-hover table-bordered">
			<thead>
				<tr>
					<th>Code</th>
					<th>Name</th>
					<th class="text-center">Is LQ45</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($stocks as $stock) 
				<tr class="{{ $stock->is_owned ? 'owned' : ($stock->is_watched ? 'watched' : '' )}}">
					<td>{{$stock->code}}</td>
					<td>{{$stock->name}}</td>
					<td class="text-center">
						@if($stock->is_lq45)
						<span class="glyphicon glyphicon-ok"></span>
						@endif
					</td>
					<td class="col-sm-1 nowrap">
						<a class="btn btn-warning btn-xs" href="{{ URL::to('stocks/'.$stock->id.'/edit') }}" >Edit</a> 
			   			{!! Form::open(array('url' => URL::to('stocks/'.$stock->id), 'class' => 'form-delete')) !!}
			   			{!! Form::hidden('_method', 'delete') !!}
			        		<button type="submit" class="btn btn-danger btn-xs">Delete</button>
			    		{!! Form::close() !!}
			    	</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@include('layout.pagination', ['page' => $stocks])
@stop