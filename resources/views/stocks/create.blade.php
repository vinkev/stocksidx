@extends('layout.master')

@section('title')
Stock - Create
@stop

@section('content')
	<h1>Create Stock</h1>
	@include('stocks.form')
@stop