@extends('layout.master')

@section('title')
Stock - Edit
@stop

@section('content')
	<h1>Edit Stock</h1>
	@include('stocks.form')
@stop