@extends('layout.master')

@section('title')
Stock Transactions
@stop

@section('content')
	<h1>List Stock Transactions</h1>
	<hr>
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="row">
				<div class="col-md-4">
				<select id="code" class="form-control" name="code" autocomplete="off">
					<option value=""></option>
					@foreach ($stocks as $item) 
					<option value="{{ $item->code }}" {{ $item->code == $stock->code ? 'selected=selected' : '' }} >{{ $item->code . " - " . $item->name }}</option>
					@endforeach
				</select>
				</div>
			</div>
		</div>
	</div>
	@yield("detail")
	<script type="text/javascript">
	$("#code").on('change', function(){
		window.location.href="{{action('StockTransactionController@index')}}/" + $(this).val();
	});
	</script>
@stop