@extends('stocktransactions.index')

@section('title')
	@parent - Detail
@stop

@section('detail')
	{!! HTML::script('js/stock.js') !!}

	<h3>{{ $stock->name }}</h3>
	<div role="tabpanel">
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active"><a href="#graph" aria-controls="graph" role="tab" data-toggle="tab">Graph</a></li>
			<li role="presentation"><a href="#table" aria-controls="table" role="tab" data-toggle="tab">Table</a></li>
		</ul>
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane fade in active" id="graph">
				<div id="chartArea" class="chartArea">
				</div>
				<script type="text/javascript">
				$("#chartArea").load("{{action('ChartController@getComplete',$stock->code)}}");
				</script>
			</div>
			<div role="tabpanel" class="tab-pane fade" id="table">
				<div class="table-responsive">
					<table class="table table-striped table-hover table-bordered">
						<thead>
							<tr>
								<th>Date</th>
								<th class="text-right">Open</th>
								<th class="text-right">High</th>
								<th class="text-right">Low</th>
								<th class="text-right">Close</th>
								<th class="text-right">Change</th>
								<th class="text-right">%</th>
								<th class="text-right">Volume</th>
								<th class="text-right">Adjust Close</th>
							</tr>
						</thead>
						<?php
							$open = 0;
							$close = 0;

						?>
						<tbody>
							@for ($i = 0; $i < count($stockTransactions); $i++) 
							<?php 
								$st = $stockTransactions[$i];
								if($i < count($stockTransactions) - 1) {
									$open = $stockTransactions[$i+1]->close_price;									
								} else {
									$open = $st->open_price;
								}
								$close = $st->close_price;
								$change = $close - $open;
								$percentage = $change / $open * 100
							?>
							<tr>
								<td>{{$st->transaction_date}}</td>
								<td class="text-right">{{number_format($st->open_price,0)}}</td>
								<td class="text-right">{{number_format($st->high_price,0)}}</td>
								<td class="text-right">{{number_format($st->low_price,0)}}</td>
								<td class="text-right">{{number_format($st->close_price,0)}}</td>
								<td class="text-right {{ $change >= 0 ? 'green' : 'red' }}">
									@if($change < 0)
										<span class="glyphicon glyphicon-triangle-bottom"></span> 
									@elseif($change > 0)
										<span class="glyphicon glyphicon-triangle-top"></span> 
									@endif
									{{number_format(abs($change),0)}}</td>
								<td class="text-right {{ $percentage >= 0 ? 'green' : 'red' }}">{{ number_format(abs($percentage),2) }}</td>
								<td class="text-right">{{number_format($st->volume,0)}}</td>
								<td class="text-right">{{number_format($st->adj_close_price,2)}}</td>
							</tr>
							@endfor
						</tbody>
					</table>
				</div>
				@include('layout.pagination', ['page' => $stockTransactions])
			</div>
		</div>
	</div>
@stop