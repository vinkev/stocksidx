@extends('layout.master')

@section('title')
	Update Data
@stop

@section('content')
	<h1>Data Update</h1>
	<hr>
	<div class="panel panel-default">
		<div class="panel-heading">
			{!! Form::open(array('url' => action('StockTransactionController@postDataUpdate'), 'class' => 'form-inline')) !!}
			{!! Form::hidden('_method', 'POST') !!}			   			
				<button type="submit" class="btn btn-warning">Update Stock Data Manually</button>
			{!! Form::close() !!}
		</div>
	</div>
@stop