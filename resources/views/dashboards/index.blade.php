@extends('layout.master')

@section('title')
IDX - Dashboards
@stop

@section('content')
	{!! HTML::script('js/stock.js') !!}

	<h1>Dashboard</h1>
	<hr/>
	<?php
		$n = 0;
		$i = 0;
		$timeout = 1000;
		$status = '';
	?>
	@foreach ($stocks as $stock)
		@if($stock->is_owned)
			@if($status != 'owned')
				@if($i > 0)
					</div>
				@endif
				<h2>Owned</h2>						
				<div class="row">
			@endif
			<?php $status = 'owned' ?>
		@elseif($stock->is_watched)
			@if($status != 'watched')
				@if($i > 0)
					</div>
				@endif
				<h2>Watched</h2>						
				<div class="row">
			@endif
			<?php $status = 'owned' ?>
		@elseif($n != $stock->trend) 
			@if($n > 0 || $i > 0)
				</div>
			@endif
			<?php $n = $stock->trend; ?>
			<h2>{{$trend[$n]}}</h2>
			<div class="row">
		@endif
		<div class="col-md-4">
			<div id="chart{{ $stock->code }}" class="chartArea">
			</div>
			<script type="text/javascript">
			setTimeout(function(){
				$.ajax({
					url: "{{action('ChartController@getCompact', $stock->code)}}",
					async: true,
					success: function(data) {
						$("#chart{{ $stock->code }}").html(data);
					}
				});
			}, ({{$i}}/4 * {{$timeout}}));
			</script>
		</div>
		<?php $i++ ?>
	@endforeach
	</div>
@stop