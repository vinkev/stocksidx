<ul class="nav nav-pills nav-stacked" role="navigation">
	<li role="presentation" class="{{ strpos(Request::path(),'dashboards') !== false ? 'active' : '' }}"><a href="{{action('DashboardController@index')}}"> <span class="glyphicon glyphicon-dashboard"></span> Dashboards</a></li>
	<li class="nav-header">Stock</li>
	<li role="presentation" class="{{ strpos(Request::path(),'stocks') !== false ? 'active' : '' }}"><a href="{{action('StockController@index')}}"> <span class="glyphicon glyphicon-sort-by-alphabet"></span> Stocks</a></li>
	<li role="presentation" class="{{ strpos(Request::path(),'stock-transactions') !== false ? 'active' : '' }}"> <a href="{{action('StockTransactionController@index')}}"><span class="glyphicon glyphicon-transfer"></span> Stock Transactions</a></li>
	<li role="presentation" class="{{ strpos(Request::path(),'stock-update') !== false ? 'active' : '' }}"> <a href="{{action('StockTransactionController@getDataUpdate')}}"><span class="glyphicon glyphicon-transfer"></span> Data Update</a></li>
    <li class="nav-header">User Management</li>
    <li role="presentation" class="{{ strpos(Request::path(),'users') !== false ? 'active' : '' }}"> <a href="{{action('UserController@index')}}"><span class="glyphicon glyphicon-user"></span> User</a></li>
    @if(Entrust::hasRole('admin'))
        <li role="presentation" class="{{ strpos(Request::path(),'roles') !== false ? 'active' : '' }}"> <a href="{{action('RoleController@index')}}"><span class="glyphicon glyphicon-list-alt"></span> Role</a></li>
    @endif
</ul>