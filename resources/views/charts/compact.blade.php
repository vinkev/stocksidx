<div id="{{ 'chartCompact-'.$code }}" class="chartCompact">
    <img class="loading" src="{{asset('/images/loading.gif')}}">
</div>
<script type="text/javascript">
$(".loading").show();
$.getJSON("{{action('StockTransactionController@getJsonp', [ 'limit' => 100, 'code' => $code])}}", function(data) {
	 // split the data set into ohlc and volume
    var ohlc = [],
        volume = [],
        close = [],
        dataLength = data.length,
        // set the allowed units for data grouping
        groupingUnits = [[
            'week',                         // unit name
            [1]                             // allowed multiples
        ]],
        i = 0;

    for (i; i < dataLength; i += 1) {
        ohlc.push([
            data[i][0], // the date
            data[i][1], // open
            data[i][2], // high
            data[i][3], // low
            data[i][4] // close
        ]);
        close.push([
            data[i][0], // the date
            data[i][4] // the volume
        ]);
    }

    // create the chart
    $("#{{ 'chartCompact-'.$code }}").highcharts('StockChart', {
        rangeSelector: {
            selected: 0,
            buttons: [{
                type: 'month',
                count: 1,
                text: '1m'
            }, {
                type: 'month',
                count: 3,
                text: '3m'
            }],
            inputEnabled: false
        },

        title: {
            text: "<a href='{{ action('StockTransactionController@show', $code) }}'>{{ $code }}</a>",
            floating: true,
            align: 'right',
            y: 17,
            x: -20,
            useHTML: true
        },

		legend: {
			enabled: false
		},

		plotOptions: {
            series: {
                marker: {
                    enabled: false,
                }
            }
        },

        tooltip: {
            crosshairs: true,
            shared: true
        },

        scrollbar: {
        	enabled: false
        },

        navigator: {
            enabled: false
        },

        exporting: {
            enabled: false
        },

        yAxis: [{
            labels: {
                align: 'right',
                x: -3
            },
            title: {
                text: 'OHLC'
            },
            height: '70%',
            lineWidth: 2,
			endOnTick: false,
			startOnTick: false
        }, {
            title: {
                text: 'MACD'
            },
            top: '75%',
            height: '25%',
            offset: 0,
            lineWidth: 2,
            plotLines: [{
            	dashStyle: 'dash',
            	value: 0,
            	width: 1,
            	color: '#000000',
            	zIndex: 1
            }], 
            tickPixelInterval: 30,
			endOnTick: false,
			startOnTick: false
        }],

        series: [{
            type: 'candlestick',
            id: '{{ $code}}',
            name: '{{ $code }}',
            data: ohlc,
            color : '#FE0002',
            upColor: '#1804ED'
        }, {
            type : 'line',
            id: 'primary',
            data : close,
            visible : false, 
            showInLegend : false
        }, {
            name: 'SMA 5',
            linkedTo: 'primary',
            showInLegend: true,
            type: 'trendline',
            algorithm: 'SMA',
            periods: 5,
            color: '#F7A35C'
        }, {
            name: 'SMA 20',
            linkedTo: 'primary',
            showInLegend: true,
            type: 'trendline',
            algorithm: 'SMA',
            periods: 20,
            color: '#90ED7D'
        }, {
            name: 'SMA 60',
            linkedTo: 'primary',
            showInLegend: true,
            type: 'trendline',
            algorithm: 'SMA',
            periods: 60,
            color: '#41EFFC'
        }, {
            name : 'MACD',
            linkedTo: 'primary',
            yAxis: 1,
            showInLegend: true,
            type: 'trendline',
            algorithm: 'MACD',
            color: '#7CB5EC',
            threshold: 0
        }, {
            name : 'Signal line',
            linkedTo: 'primary',
            yAxis: 1,
            showInLegend: true,
            type: 'trendline',
            algorithm: 'signalLine',
            dashStyle: 'shortdot',
            color: '#D3CD0E'

        }, {
            name: 'Histogram',
            linkedTo: 'primary',
            yAxis: 1,
            showInLegend: true,
            type: 'histogram',
            color: '#1804ED',
            negativeColor: '#FE0002',
            threshold: 0
			}]
    });

});
</script>