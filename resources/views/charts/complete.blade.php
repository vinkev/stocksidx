<div id="{{ 'chartComplete-'.$code }}" class="chartComplete">
	<img class="loading" src="{{asset('/images/loading.gif')}}">
</div>
<script type="text/javascript">
$(".loading").show();
$.getJSON("{{action('StockTransactionController@getJsonp', $code)}}", function(data) {
	 // split the data set into ohlc and volume
    var ohlc = [],
        volume = [],
        close = [],
        dataLength = data.length,
        // set the allowed units for data grouping
        groupingUnits = [[
            'week',                         // unit name
            [1]                             // allowed multiples
        ], [
            'month',
            [1, 2, 3, 4, 6]
        ]],

        i = 0;

    for (i; i < dataLength; i += 1) {
        ohlc.push([
            data[i][0], // the date
            data[i][1], // open
            data[i][2], // high
            data[i][3], // low
            data[i][4] // close
        ]);

        volume.push([
            data[i][0], // the date
            data[i][5] // the volume
        ]);

        close.push([
            data[i][0], // the date
            data[i][4] // the volume
        ]);
    }

    // create the chart
    $("#{{ 'chartComplete-'.$code }}").highcharts('StockChart', {
        rangeSelector: {
            selected: 1
        },

        title: {
            text: '{{$code}} Historical'
        },

		legend: {
			enabled: true,
			align: 'right',
			verticalAlign: 'middle',
			layout: 'vertical'
		},

		plotOptions: {
            series: {
                marker: {
                    enabled: false,
                },
                events: {
                    legendItemClick: function() {
                        return false;
                    }
                }
            }
        },

        tooltip: {
            crosshairs: true,
            shared: true
        },

        scrollbar: {
        	enabled: false
        },

        yAxis: [{
            labels: {
                align: 'right',
                x: -3
            },
            title: {
                text: 'OHLC'
            },
            height: '60%',
            lineWidth: 2,
			endOnTick: false,
			startOnTick: false
        }, {
            title: {
                text: 'MACD'
            },
            top: '65%',
            height: '20%',
            offset: 0,
            lineWidth: 2,
            plotLines: [{
            	dashStyle: 'dash',
            	value: 0,
            	width: 1,
            	color: '#000000',
            	zIndex: 1
            }], 
            tickPixelInterval: 30,
			endOnTick: false,
			startOnTick: false
        }, {
            labels: {
                align: 'right',
                x: -3
            },
            title: {
                text: 'Volume'
            },
            top: '85%',
            height: '15%',
            offset: 0,
            lineWidth: 2,
			endOnTick: false,
			startOnTick: false
        }],

        series: [{
            type: 'candlestick',
            id: '{{ $code}}',
            name: '{{ $code }}',
            data: ohlc,
            dataGrouping: {
                units: groupingUnits
            },
            color : '#FE0002',
            upColor: '#1804ED'
        }, {
            type: 'column',
            name: 'Volume',
            data: volume,
            yAxis: 2,
            dataGrouping: {
                units: groupingUnits
            }
        }, {
            type : 'line',
            id: 'primary',
            data : close,
            visible : false, 
            showInLegend : false
        }, {
            name: 'SMA 5',
            linkedTo: 'primary',
            showInLegend: true,
            type: 'trendline',
            algorithm: 'SMA',
            periods: 5,
            color: '#F7A35C'
        }, {
            name: 'SMA 20',
            linkedTo: 'primary',
            showInLegend: true,
            type: 'trendline',
            algorithm: 'SMA',
            periods: 20,
            color: '#90ED7D'
        }, {
            name: 'SMA 60',
            linkedTo: 'primary',
            showInLegend: true,
            type: 'trendline',
            algorithm: 'SMA',
            periods: 60,
            color: '#41EFFC'
        }, {
            name : 'MACD',
            linkedTo: 'primary',
            yAxis: 1,
            showInLegend: true,
            type: 'trendline',
            algorithm: 'MACD',
            color: '#7CB5EC',
            threshold: 0
        }, {
            name : 'Signal line',
            linkedTo: 'primary',
            yAxis: 1,
            showInLegend: true,
            type: 'trendline',
            algorithm: 'signalLine',
            dashStyle: 'shortdot',
            color: '#D3CD0E'

        }, {
            name: 'Histogram',
            linkedTo: 'primary',
            yAxis: 1,
            showInLegend: true,
            type: 'histogram',
            color: '#1804ED',
            negativeColor: '#FE0002',
            threshold: 0
		}]
    });

});
</script>