<?php 
if($user == "[]") {
	$formUrl = action("UserController@store");
	$method = "post";
} else {
	$formUrl = action("UserController@update", $user->id);
	$method = "put";
}
?>
<div class="well">
	{!! Form::open(array('url' => $formUrl, 'class' => 'form form-horizontal')) !!}
	{!! Form::hidden('_method',$method) !!}
	<div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
		{!! Form::label("name", "Name", array('class' => 'col-md-2 control-label'))!!}
		<div class="col-md-10">
			{!! Form::text('name', $user->name, ['class' => 'form-control', 'placeholder' => 'Name', 'autofocus' => 'autofocus']) !!}
			@if ($errors->has('name'))
			{!! $errors->first('name', '<small class=error>:message</small>') !!}
			@endif
		</div>
	</div>
	<div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
		{!! Form::label("email", "Email", ['class' => 'col-md-2 control-label'])!!}
		<div class="col-md-10">
			{!! Form::text('email', $user->email, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
			@if ($errors->has('email'))
			{!! $errors->first('email', '<small class=error>:message</small>') !!}
			@endif
		</div>
	</div>
	@if (Entrust::hasRole('admin'))
	<div class="form-group {!! $errors->has('roles') ? 'has-error' : '' !!}">
		{!! Form::label("role", "Role", ['class' => 'col-md-2 control-label'])!!}
		<div class="col-md-3">
			<select name="role" class="form-control">
				@foreach ($roles as $role)
				<option value="{{$role->id}}" {{ $user->hasRole($role->name) ? 'selected' : '' }}>{{$role->display_name}}</option>
				@endforeach
			</select>
		</div>
	</div>
	@endif
    <div class="form-group">
        <div class="col-md-2"></div>
        <div class="col-md-10"><hr/></div>
    </div>
    @if ($method == 'put')
    <div class="form-group {!! $errors->has('current_password') ? 'has-error' : '' !!}">
        {!! Form::label("current_password", "Current Password", ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-5">
            {!! Form::password('current_password', ['class' => 'form-control', 'placeholder' => 'Current Password']) !!}
            @if ($errors->has('current_password'))
                {!! $errors->first('current_password', '<small class=error>:message</small>') !!}
            @endif
        </div>
    </div>
    @endif
    <div class="form-group {!! $errors->has('new_password') ? 'has-error' : '' !!}">
        {!! Form::label("new_password", $method=='put' ? "New Password" : 'Password', ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-5">
            {!! Form::password('new_password', ['class' => 'form-control', 'placeholder' => $method=='put' ? "New Password" : 'Password']) !!}
            @if ($errors->has('new_password'))
                {!! $errors->first('new_password', '<small class=error>:message</small>') !!}
            @endif
        </div>
    </div>
    <div class="form-group {!! $errors->has('new_password_confirmation') ? 'has-error' : '' !!}">
        {!! Form::label("new_password_confirmation", "Password Confirm", ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-5">
            {!! Form::password('new_password_confirmation', ['class' => 'form-control', 'placeholder' => 'Password Confirm']) !!}
            @if ($errors->has('new_password_confirmation'))
                {!! $errors->first('new_password_confirmation', '<small class=error>:message</small>') !!}
            @endif
        </div>
    </div>
	<div class="form-group">
		<div class="col-md-offset-2 col-md-10">
			{!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
			<a class="btn btn-danger" href="{{ action('UserController@index')}}">Cancel</a>
		</div>
	</div>
	{!! Form::close() !!}
</div>