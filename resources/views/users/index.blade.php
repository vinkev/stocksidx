@extends('layout.master')

@section('title')
Users
@stop

@section('content')
	<h1>List Users</h1>
	@if(Entrust::hasRole("admin"))
	<hr>
	<div class="panel panel-default">
		<div class="panel-heading">
			<a class="btn btn-primary" href="{{ action('UserController@create')}}">Add New User</a>
		</div>
	</div>
	@endif

	<div class="table-responsive">
		<table class="table table-striped table-hover table-bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th>Email</th>
                    <th>Role</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($users as $user)
				<tr>
					<td>{{$user->name}}</td>
					<td>{{$user->email}}</td>
                    <td>{{count($user->roles) > 0 ? $user->roles[0]->display_name : ''}}</td>
					<td class="col-sm-1 nowrap">
                        @if(Entrust::user()->id == $user->id)
						<a class="btn btn-warning btn-xs" href="{{ action('UserController@edit', $user->id) }}" >Edit</a>
                        @endif
			   			@if(Entrust::hasRole("admin"))
                        {!! Form::open(array('url' => action('UserController@destroy', $user->id), 'class' => 'form-delete')) !!}
			   			{!! Form::hidden('_method', 'delete') !!}
			        		<button type="submit" class="btn btn-danger btn-xs">Delete</button>
		    		    {!! Form::close() !!}
                        @endif
			    	</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	@include('layout.pagination', ['page' => $users])
@stop