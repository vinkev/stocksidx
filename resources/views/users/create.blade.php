@extends('layout.master')

@section('title')
User - Create
@stop

@section('content')
	<h1>Create User</h1>
	@include('users.form')
@stop