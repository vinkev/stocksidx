<?php namespace SahamIDX\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use SahamIDX\Scheduler\StockScheduler;
use Carbon\Carbon;

class StockTask extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'stock:update';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Update all stock code with latest daily transaction data';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		echo Carbon::now()->toDateTimeString() . PHP_EOL;
		echo "##### Stock update started #####" . PHP_EOL . PHP_EOL;
		StockScheduler::updateStocks();
		echo "##### Stock update finished #####" . PHP_EOL . PHP_EOL;
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return [
			// ['example', InputArgument::REQUIRED, 'An example argument.'],
		];
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return [
			// ['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
		];
	}

}
