<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'auth/login', 'uses' => 'Auth\AuthController@getLogin']);

Route::get('/', 'HomeController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::resource('dashboards', 'DashboardController', ['only' => ['index', 'show']]);

Route::resource('stocks', 'StockController', ['except' => 'show']);

Route::get('stock-transactions/{code}/jsonp', 'StockTransactionController@getJsonp');
Route::get('stock-transactions/data-update', 'StockTransactionController@getDataUpdate');
Route::post('stock-transactions/data-update', 'StockTransactionController@postDataUpdate');
Route::resource('stock-transactions', 'StockTransactionController', ['only' => ['index', 'show']]);

Route::get('charts/{code}/compact', 'ChartController@getCompact');
Route::get('charts/{code}/complete', 'ChartController@getComplete');

Route::resource('users', 'UserController');
Route::resource('roles', 'RoleController', ['except' => 'show']);

Entrust::routeNeedsRole('roles*', 'admin', Redirect::to(action('HomeController@index')));
