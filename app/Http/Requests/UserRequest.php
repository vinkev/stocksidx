<?php namespace SahamIDX\Http\Requests;

use SahamIDX\Http\Requests\Request;
use SahamIDX\Model\User;

class UserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = [
			'name' => 'required',
			'email' => ['required', 'email'],
			'new_password' => ['min:8', 'confirmed'],
			'new_password_confirmation' => 'required_with:new_password'
		];

		$rule = 'unique:users,email';

		if($this->method() == 'PUT') {
			$segments = $this->segments();
			$id = intval(end($segments));
			$rule .= "," . $id . ",id,deleted_at,NULL";
			$rules['new_password'][] = 'different:current_password';
			$rules['new_password'][] = 'required_with:current_password';
		} else {
			$rule .= ",NULL,id,deleted_at,NULL";
			$rules['new_password'][] = 'required';
		}
		$rules['email'][] = $rule;
		
		return $rules;
	}

}
