<?php namespace SahamIDX\Http\Requests;

use SahamIDX\Http\Requests\Request;

class StockRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'code' => 'required|min:4',
			'name' => 'required'
		];
	}

	public function message()
	{

	}

}
