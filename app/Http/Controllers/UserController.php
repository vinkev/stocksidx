<?php namespace SahamIDX\Http\Controllers;

use SahamIDX\Http\Requests\UserRequest;
use SahamIDX\Http\Controllers\Controller;
use SahamIDX\Model\User;
use SahamIDX\Model\Role;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller {

	public function __construct() {
		$this->middleware("auth");
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::orderBy('name')->paginate(40);
		return view('users.index',compact('users'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		if(!Auth::user()->hasRole('admin')) {
			return redirect(action('UserController@index'));
		}

		$user = new User();
		$roles = Role::orderBy('display_name')->get();
		return view('users.create', compact('user', 'roles'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UserRequest $request)
	{
		if(!Auth::user()->hasRole('admin')) {
			return redirect(action('UserController@index'));
		}

		$user = new User();
		$user->name = $request->name;
		$user->email = $request->email;
		$user->password = Hash::make($request->new_password);
        $user->save();
        $user->roles()->attach($request->role);

		$request->flash();
		return redirect(action('UserController@index'));
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::findOrFail($id);
		$roles = Role::orderBy('display_name')->get();
		return view('users.edit', compact('user', 'roles'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(UserRequest $request, $id)
	{
		$user = User::findOrFail($id);

		if($request->current_password != '') {
			if(!Hash::check($request->current_password, $user->password)) {
			return redirect(action('UserController@edit', $id))
				->withErrors(['current_password'=>'Wrong password'])
				->withInput($request->except('current_password'));
			}
			$user->password = Hash::make($request->current_password);
		}

		$user->name = $request->name;
		$user->email = $request->email;
        $user->roles()->detach();
        $user->roles()->attach($request->role);

		$user->save();
		$request->flash();
		return redirect('users');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		User::destroy($id);
		return redirect('users');
	}

}
