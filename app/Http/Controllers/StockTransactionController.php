<?php namespace SahamIDX\Http\Controllers;

use SahamIDX\Http\Requests;
use SahamIDX\Http\Controllers\Controller;
use SahamIDX\Model\Stock;
use SahamIDX\Model\StockTransaction;
use SahamIDX\Scheduler\StockScheduler;
use Illuminate\Http\Request;
use Carbon\Carbon;

class StockTransactionController extends Controller {

	/**
	 * All stocks repository instance
	 * @var [type]
	 */
	private $stocks;

	public function __construct() {
		$this->stocks = Stock::orderBy("code", "asc")->get();			
		$this->middleware("auth");
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view("stocktransactions.index", [
			"stocks" => $this->stocks,
			"stock" => new Stock()
		]);
	}

	public function show($code)
	{
		$stock = Stock::whereCode($code)->first();
		if($stock == null) {
			abort(404);	
		}

		$stockTransactions = StockTransaction::whereStockId($stock->id)
				->orderBy("transaction_date", "desc")->paginate(100);
		
		return view("stocktransactions.show", [
			"stocks" => $this->stocks,
			"stock" => $stock,
			"stockTransactions" => $stockTransactions
		]);
	}

	public function getJsonp(Request $request, $code)
	{
		$stock = Stock::whereCode($code)->first();
		if($stock == null) {
			abort(404);	
		}
		$limit = $request->has('limit') ? $request->query('limit') : 500 ;

		$stockTransactions = StockTransaction::whereStockId($stock->id)
				->where("volume", "!=", 0)
				->orderBy("transaction_date", "desc")->take($limit)->get();
		
		$stArray = [];
		foreach ($stockTransactions as $stockTransaction) {
			$st = [];
			array_push($st, strtotime($stockTransaction->transaction_date) * 1000);
			array_push($st, $stockTransaction->open_price);
			array_push($st, $stockTransaction->high_price);
			array_push($st, $stockTransaction->low_price);
			array_push($st, $stockTransaction->close_price);
			array_push($st, $stockTransaction->volume);
			array_unshift($stArray, $st);
		}

		return response()->json($stArray, 200, ['Content-Type' => 'application/json'], JSON_NUMERIC_CHECK)
						 ->setCallback($request->input('callback'));
	}

	function getDataUpdate() {
		return view("stocktransactions.update");
	}

	function postDataUpdate() {
		dd(Carbon::now());
		StockScheduler::updateStocks();
	}

}
