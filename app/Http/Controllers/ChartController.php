<?php namespace SahamIDX\Http\Controllers;

use SahamIDX\Http\Requests;
use SahamIDX\Http\Controllers\Controller;
use SahamIDX\Model\StockTransaction;
use Illuminate\Http\Request;

class ChartController extends Controller {

	function getCompact($code) {
		return view('charts.compact', compact('code'));
	}

	function getComplete($code) {
		return view('charts.complete', compact('code'));
	}
}
