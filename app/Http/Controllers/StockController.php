<?php namespace SahamIDX\Http\Controllers;

use SahamIDX\Http\Requests\StockRequest;
use SahamIDX\Http\Controllers\Controller;
use SahamIDX\Model\Stock;

use Illuminate\Http\RedirectResponse;

class StockController extends Controller {

	public function __construct() {
		$this->middleware("auth");
	}

	/**
	 * 
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$stocks = Stock::orderBy('is_owned', 'desc')
					->orderBy('is_watched', 'desc')
					->orderBy('is_lq45', 'desc')
					->orderBy('code', 'asc')->paginate(40);
		return view('stocks.index',compact('stocks'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$stock = new Stock();
		return view('stocks.create', compact('stock'));
	}

	/**
	 *  Store a newly created resource in storage.	
	 * @param  StockRequest $request [description]
	 * @return redirect                [description]
	 */
	public function store(StockRequest $request)
	{
		if(count(Stock::whereCode($request->code)->get())>0){
			return redirect('stocks/create')
			->withErrors(['code'=>'This code is already used, please use another code.'])
			->withInput($request->all);
		}
		$stock = new Stock();
		$stock->name = $request->name;
		$stock->code = strtoupper($request->code);
		$stock->is_owned = $request->is_owned ? true : false;
		$stock->is_watched = $request->is_watched ? true : false;
		$stock->is_lq45 = $request->is_lq45 ? true : false;
		$stock->save();
		$request->flash();
		return redirect('stocks');
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$stock = Stock::findOrFail($id);
		return view('stocks.edit', compact('stock'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(StockRequest $request, $id)
	{
		$stock = Stock::findOrFail($id);
		if($stock->code != $request->code) {
			if(count(Stock::whereCode($request->code)->get())>0){
				return redirect('stocks/'.$id.'/edit')
				->withErrors(['code'=>'This code is already used, please use another code.'])
				->withInput($request->all);
			}
		}
		$stock->name = $request->name;
		$stock->code = strtoupper($request->code);
		$stock->is_owned = $request->is_owned ? true : false;
		$stock->is_watched = $request->is_watched ? true : false;
		$stock->is_lq45 = $request->is_lq45 ? true : false;

		$stock->save();
		$request->flash();
		return redirect('stocks');

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Stock::destroy($id);
		return redirect('stocks');
	}

}

?>