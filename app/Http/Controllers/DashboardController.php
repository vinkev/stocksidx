<?php namespace SahamIDX\Http\Controllers;

use SahamIDX\Http\Requests;
use SahamIDX\Http\Controllers\Controller;
use SahamIDX\Model\Stock;
use Illuminate\Http\Request;

class DashboardController extends Controller {

	public function __construct() {
		$this->middleware("auth");
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$stocks = Stock::whereIsLq45(true)
				->orWhere('is_owned', true)
				->orWhere('is_watched', true)
				->orderBy('is_owned','desc')
				->orderBy('is_watched','desc')				
				->orderBy('trend','asc')
				->orderBy('code', 'asc')->get();

		$trend[1] = 'Up Trend';
		$trend[2] = 'Down Trend';
		
		return view('dashboards.index', compact('stocks','trend'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
