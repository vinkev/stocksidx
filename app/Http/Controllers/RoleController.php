<?php namespace SahamIDX\Http\Controllers;

use SahamIDX\Http\Requests;
use SahamIDX\Http\Requests\RoleRequest;
use SahamIDX\Http\Controllers\Controller;
use SahamIDX\Model\Role;

use Illuminate\Http\Request;

class RoleController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$roles = Role::orderBy('name')->paginate(20);
		return view("roles.index", compact('roles'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$role = new Role();
		return view("roles.create", compact("role"));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(RoleRequest $request)
	{
		$role = new Role();
		$role->name = $request->name;
		$role->display_name = $request->display_name;
		$role->description = $request->description;
		$role->save();

		$request->flash();
		return redirect(action('RoleController@index'));

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$role = Role::findOrFail($id);
		return view('roles.edit', compact('role'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(RoleRequest $request, $id)
	{
		$role = Role::findOrFail($id);
        $role->name = $request->name;
        $role->display_name = $request->display_name;
        $role->description = $request->description;
        $role->save();

        $request->flash();
        return redirect(action('RoleController@index'));
    }

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        Role::destroy($id);
        return redirect(action('RoleController@index'));
	}
}
