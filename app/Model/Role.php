<?php namespace SahamIDX\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\EntrustRole;

/**
 * SahamIDX\Model\Role
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Config::get('auth.model')[] $users
 * @property-read \Illuminate\Database\Eloquent\Collection|\Config::get('entrust.permission')[] $perms
 * @property integer $id
 * @property string $name
 * @property string $display_name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Role whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Role whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Role whereDisplayName($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Role whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Role whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Role whereDeletedAt($value)
 */
class Role extends EntrustRole {

	use SoftDeletes;
    protected $dates = ['deleted_at'];

}
