<?php namespace SahamIDX\Model;

use SahamIDX\Model\BaseModel;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;

/**
 * SahamIDX\Model\User
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Config::get('entrust.role')[] $roles
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\User whereUpdatedAt($value)
 * @property \Carbon\Carbon $deleted_at 
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\User whereDeletedAt($value)
 */
class User extends BaseModel implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, EntrustUserTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

}
