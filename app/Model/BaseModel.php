<?php namespace SahamIDX\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * SahamIDX\Model\BaseModel
 *
 */
class BaseModel extends Model {

	use SoftDeletes;
    protected $dates = ['deleted_at'];

}
