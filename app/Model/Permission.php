<?php namespace SahamIDX\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Zizaco\Entrust\EntrustPermission;

/**
 * SahamIDX\Model\Permission
 *
 * @property integer $id
 * @property string $name
 * @property string $display_name
 * @property string $description
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Config::get('entrust.role')[] $roles
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Permission whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Permission whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Permission whereDisplayName($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Permission whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Permission whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Permission whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Permission whereDeletedAt($value)
 */
class Permission extends EntrustPermission {

	use SoftDeletes;
    protected $dates = ['deleted_at'];

}
