<?php namespace SahamIDX\Model;

use SahamIDX\Model\BaseModel;

/**
 * SahamIDX\Model\Stock
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property boolean $is_lq45
 * @property boolean $is_watched
 * @property boolean $is_owned
 * @property boolean $trend
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Stock whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Stock whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Stock whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Stock whereIsLq45($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Stock whereIsWatched($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Stock whereIsOwned($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Stock whereTrend($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Stock whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Stock whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\Stock whereDeletedAt($value)
 */
class Stock extends BaseModel {

	protected $table = 'stocks';

}
