<?php namespace SahamIDX\Model;

use SahamIDX\Model\Stock;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


/**
 * SahamIDX\Model\StockTransaction
 *
 * @property integer $id
 * @property integer $stock_id
 * @property \Carbon\Carbon $transaction_date
 * @property float $open_price
 * @property float $high_price
 * @property float $low_price
 * @property float $close_price
 * @property float $volume
 * @property float $adj_close_price
 * @property float $sma_5
 * @property float $sma_20
 * @property float $sma_60
 * @property float $macd_value
 * @property float $signal_value
 * @property float $histogram_value
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \SahamIDX\Model\Stock $stock
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\StockTransaction whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\StockTransaction whereStockId($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\StockTransaction whereTransactionDate($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\StockTransaction whereOpenPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\StockTransaction whereHighPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\StockTransaction whereLowPrice($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\StockTransaction whereClosePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\StockTransaction whereVolume($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\StockTransaction whereAdjClosePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\StockTransaction whereSma5($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\StockTransaction whereSma20($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\StockTransaction whereSma60($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\StockTransaction whereMacdValue($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\StockTransaction whereSignalValue($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\StockTransaction whereHistogramValue($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\StockTransaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\SahamIDX\Model\StockTransaction whereUpdatedAt($value)
 */
class StockTransaction extends Model {

	protected $table = 'stock_transactions';

	protected $dates = ['transaction_date'];

	public function stock()
    {
        return $this->belongsTo('SahamIDX\Model\Stock');
    }

    public function setTransactionDateAttribute($date) {
		$this->attributes['transaction_date'] = Carbon::parse($date);
    }

    public function getTransactionDateAttribute() {
		return Carbon::createFromTimeStamp(strtotime($this->attributes['transaction_date']))->toDateString();
    }

}
