<?php namespace SahamIDX\Scheduler;

use SahamIDX\Model\Stock;
use SahamIDX\Model\StockTransaction;
use DB;
use Carbon\Carbon;

class StockScheduler {
	public static function updateStocks(){
		foreach (Stock::all() as $stock) {
			echo "*** Processing " . $stock->code . " ***" . PHP_EOL;
			echo Carbon::now()->toDateTimeString() . PHP_EOL;

			$jsonString = file_get_contents("http://www.dwsec-id.com/tr/cpstChartAjaxTR.do?StockCode=".$stock->code."&periodBit=I");
			$start = microtime(true); 
			echo "JSON data downloaded" . PHP_EOL;

			$json = json_decode($jsonString);
			$lines = explode('&', $json->B);

			$lines = array_filter($lines);
			$lines = array_values($lines);
			$lines = array_reverse($lines);

			$stockTransaction = StockTransaction::whereStockId($stock->id)
								->orderBy("transaction_date", "DESC")->first();
			$lastdate = null;
			if($stockTransaction) {
				$lastdate = $stockTransaction->getTransactionDateAttribute();
			}
			echo "DB last date : " . $lastdate . PHP_EOL;

			/**
			 * put stock data to db
			 */
 			DB::beginTransaction(); 
 			$count = 0;
			for($i=0; $i<count($lines); $i++) {
				$data = explode('|', $lines[$i]);

				$date = Carbon::createFromFormat('Ymd',$data[0])->toDateString();
				if($lastdate == $date) {
					break;
				}

				DB::insert("INSERT INTO stock_transactions (stock_id, transaction_date, open_price, high_price, low_price, close_price, volume, created_at, updated_at) " .
					"VALUES (?, ?, ?, ?, ?, ?, ?, NOW(), NOW())", [$stock->id , $date, (int)$data[2], (int)$data[3], (int)$data[4], (int)$data[5], (int)$data[9]]);
				$count++;
			}
			DB::commit();

			echo "Remote data imported" . PHP_EOL;
			echo "Starting calculate technical indicator" . PHP_EOL;

			self::calculateTehcnicalIndicator($stock, $count);

			echo "Starting calculate trend" . PHP_EOL;
			self::calculateTrend($stock,1);

			echo "Finished calculating" . PHP_EOL;
			
			$end = microtime(true) - $start;
			echo "Insert & process [".($count)."] new data in $end seconds". PHP_EOL;
			echo "*** " . $stock->code . " successfully updated ***" . PHP_EOL . PHP_EOL;
		}
	}

	private static function calculateTehcnicalIndicator($stock, $count) {
		/**
		 * populate technical indicator data
		 */
		if($count > 0) {
			$closePrices = StockTransaction::whereStockId($stock->id)
								->orderBy("transaction_date", "ASC")->lists('close_price');
			$transactionDates = StockTransaction::whereStockId($stock->id)
								->orderBy("transaction_date", "ASC")->lists('transaction_date');
			$ma5s = trader_ma($closePrices, 5, TRADER_MA_TYPE_SMA);
			$ma20s = trader_ma($closePrices, 20, TRADER_MA_TYPE_SMA);								
			$ma60s = trader_ma($closePrices, 60, TRADER_MA_TYPE_SMA);
			$macds = trader_macd($closePrices, 12, 26, 9);
			$countClose = count($closePrices);
			$countMacd = count($macds[0]);

			$data = [];
			for($i=0; $i<$countClose;$i++){
				$line = [];
				$ma5 = array_key_exists($i, $ma5s) ? $ma5s[$i] : null ; 
				$ma20 = array_key_exists($i, $ma20s) ? $ma20s[$i] : null ; 
				$ma60 = array_key_exists($i, $ma60s) ? $ma60s[$i] : null ; 
				$macd = $countMacd - $countClose + $i >=0 ? $macds[0][$i] : null ;
				$signal = $countMacd - $countClose + $i >=0 ? $macds[1][$i] : null ;
				$histogram = $countMacd - $countClose + $i >=0 ? $macds[2][$i] : null ;

				$line["date"] = $transactionDates[$i];
				$line["ma5"] = $ma5;
				$line["ma20"] = $ma20;
				$line["ma60"] = $ma60;
				$line["macd"] = $macd;
				$line["signal"] = $signal;
				$line["histogram"] = $histogram;
				array_push($data, $line);			
			}
			$data = array_reverse($data);

			/**
			 * update stock transaction db with technical data
			 */	
			DB::beginTransaction(); 		
			for($i=0; $i<$count; $i++) {
				DB::update("UPDATE stock_transactions " .
					"SET sma_5 = ?, " .
					"sma_20 = ?, " .
					"sma_60 = ?, " .
					"macd_value = ?, " .
					"signal_value = ?, " .
					"histogram_value = ? " .
					"WHERE transaction_date = ? AND stock_id = ?",
					[$data[$i]['ma5'],
				  	$data[$i]['ma20'],
				  	$data[$i]['ma60'], 
				  	$data[$i]['macd'], 
				  	$data[$i]['signal'], 
				  	$data[$i]['histogram'], 
				  	$data[$i]['date'],
				  	$stock->id]);
			}
			DB::commit();				
		}
	}

	private static function calculateTrend($stock, $count){
		if($count > 0) {
			$ma = StockTransaction::select('transaction_date', 'sma_20', 'sma_60')
						->whereStockId($stock->id)
						->orderBy("transaction_date", "DESC")->take(6)->get()->toArray();

			// count difference
			for($i=0; $i<count($ma); $i++){
				$ma[$i]['diff'] = $ma[$i]['sma_20'] - $ma[$i]['sma_60'];
			}

			// count difference of difference
			$initup = 0;
			$up = 0;
			$upDiff = 0;
			for($i=0; $i<count($ma)-1; $i++){
				$ma[$i]['diff2'] = $ma[$i]['diff']-$ma[$i+1]['diff'];
				if($ma[$i]['diff'] > 0) {
					$up++;
				}
				if($ma[$i]['diff2'] > 0){
				 	$upDiff++;
				 	if($i<2 && abs($ma[$i]['diff']) * 0.05 < $ma[$i]['diff2']) {
						$initup++;
					}

				}
				
			}

			$trend = 2;
			if($initup >= 2 || $upDiff >= 4){
				$trend = 1;
			}

			Stock::whereId($stock->id)
				->update(['trend'=>$trend]);
		}
	}
}