<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('stocks', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 255);
			$table->string('code')->unique();
			$table->boolean('is_lq45');
			$table->boolean('is_watched');
			$table->boolean('is_owned');
			$table->tinyIntegers('trend'); // 1:uptrend, 2:downtrend
			$table->timestamps();
			$table->softDeletes();
		});	
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('stocks');
	}

}
