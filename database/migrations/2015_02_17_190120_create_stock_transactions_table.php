<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('stock_transactions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('stock_id')->unsigned()->index();
			$table->foreign('stock_id')->references('id')->on('stocks')->onDelete('cascade');
			$table->timestamp('transaction_date')->index();
			$table->decimal('open_price',10,2);
			$table->decimal('high_price',10,2);
			$table->decimal('low_price',10,2);
			$table->decimal('close_price',10,2);
			$table->decimal('volume',15,2);
			$table->decimal('adj_close_price',10,2)->nullable();
			$table->decimal('sma_5',10,2)->nullable();
			$table->decimal('sma_20',10,2)->nullable();
			$table->decimal('sma_60',10,2)->nullable();
			$table->decimal('macd_value',10,2)->nullable();
			$table->decimal('signal_value',10,2)->nullable();
			$table->decimal('histogram_value',10,2)->nullable();
			$table->timestamps();
		});		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('stock_transactions');
	}

}
